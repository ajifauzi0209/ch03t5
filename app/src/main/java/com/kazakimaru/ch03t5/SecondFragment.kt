package com.kazakimaru.ch03t5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import com.kazakimaru.ch03t5.FirstFragment.Companion.NAMA
import com.kazakimaru.ch03t5.databinding.FragmentSecondBinding


class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    companion object {
        const val MY_NAME = "name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            val getNama = arguments?.getString(NAMA)
            val bundle = Bundle()
            bundle.putString(NAMA, getNama)
            setFragmentResult("requestKey", bundle)
            requireActivity().findNavController(R.id.fragmentContainerView).popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // get nama
        val getNama = arguments?.getString(NAMA)

//        _binding?.txtNama?.text = "Nama: $getNama"
        // atau
        binding.txtNama.text = "Nama: $getNama"

        binding.btnTo3.setOnClickListener {
            if(binding.editNama.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Tidak boleh kosong", Toast.LENGTH_SHORT).show()
            } else {
                val getEditNama = binding.editNama.text.toString()
                val getEditAlamat = binding.editAlamat.text.toString()

//                caraPertama(getEditNama, it)
                caraKedua(getEditNama, getEditAlamat, it)
            }
        }

        binding.btnBack1.setOnClickListener {
            sendDataBackToPrevious(getNama, it)
        }
    }

    private fun caraPertama(valueFromEditTextNama: String, it: View) {
        val mBundle = Bundle()
        mBundle.putString(MY_NAME, valueFromEditTextNama)
        it.findNavController().navigate(R.id.action_secondFragment_to_thirdFragment, mBundle)
    }

    private fun caraKedua(valueFromEditTextNama: String, valueFromEditTextAlamat: String, it: View) {
        val mahashiwa = Mahasiswa("Aji F", 187000)
        val action = SecondFragmentDirections.actionSecondFragmentToThirdFragment(
            namaku =  valueFromEditTextNama,
            alamatku =  valueFromEditTextAlamat,
            keyParcel = mahashiwa
        )
        it.findNavController().navigate(action)
    }

    private fun sendDataBackToPrevious(dataNama: String?, it: View) {
        val bundle = Bundle()
        bundle.putString(NAMA, dataNama)
        setFragmentResult("requestKey", bundle)
        it.findNavController().popBackStack()
    }
}
