package com.kazakimaru.ch03t5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.kazakimaru.ch03t5.SecondFragment.Companion.MY_NAME
import com.kazakimaru.ch03t5.databinding.FragmentThirdBinding


class ThirdFragment : Fragment() {

    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // get nama
        // cara Pertama
//        val getNama = arguments?.getString(MY_NAME)
        // cara Kedua
        val getNama = ThirdFragmentArgs.fromBundle(arguments as Bundle).namaku
        val getAlamat = ThirdFragmentArgs.fromBundle(arguments as Bundle).alamatku
        val getMhs = ThirdFragmentArgs.fromBundle(arguments as Bundle).keyParcel
//        _binding?.txtNama?.text = getNama
        binding.txtNama.text = "Nama: ${getNama}"
        binding.txtAlamat.text = "Alamat: ${getAlamat}"
        binding.txtNamamhs.text = "Nama Mhs: " + getMhs.nama
        binding.txtNpm.text = "NPM: " + getMhs.npm.toString()

    }
}