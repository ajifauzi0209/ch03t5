package com.kazakimaru.ch03t5

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Mahasiswa(
    val nama: String,
    val npm: Int
) : Parcelable